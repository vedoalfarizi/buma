<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDrinkingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('drinkings', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('user_id')->unsigned();
            $table->string('location', 100);
            $table->double('bau', 8, 3);
            $table->double('warna', 8, 3);
            $table->double('tds', 8, 3);
            $table->double('kekeruhan', 8, 3);
            $table->double('rasa', 8, 3);
            $table->double('suhu', 8, 3);
            $table->double('ecoli', 8, 3);
            $table->double('coliform', 8, 3);
            $table->double('aluminium', 8, 3);
            $table->double('besi', 8, 3);
            $table->double('kesadahan', 8, 3);
            $table->double('klorida', 8, 3);
            $table->double('mangan', 8, 3);
            $table->double('pH', 8, 3);
            $table->double('seng', 8, 3);
            $table->double('sulfat', 8, 3);
            $table->double('tembaga', 8, 3);
            $table->double('amonia', 8, 3);
            $table->double('arsen', 8, 3);
            $table->double('flourida', 8, 3);
            $table->double('kromium', 8, 3);
            $table->double('kadmium', 8, 3);
            $table->double('nitrit', 8, 3);
            $table->double('nitrat', 8, 3);
            $table->double('sianida', 8, 3);
            $table->double('selenium', 8, 3);
            $table->string('month', 10);
            $table->timestamps();

            $table->foreign('user_id')->references('id')->on('users')->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('drinkings');
    }
}
