<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWastesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('wastes', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('user_id')->unsigned();
            $table->string('location', 100);
            $table->double('pH', 8, 3);
            $table->double('bod', 8, 3);
            $table->double('cod', 8, 3);
            $table->double('tss', 8, 3);
            $table->double('oil', 8, 3);
            $table->double('amoniak', 8, 3);
            $table->double('coliform', 8, 3);
            $table->string('month', 10);
            $table->timestamps();

            $table->foreign('user_id')->references('id')->on('users')->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('wastes');
    }
}
