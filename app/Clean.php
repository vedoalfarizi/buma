<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Clean extends Model
{
    protected $fillable = [
        'user_id', 'location', 'kekeruhan', 'warna', 'tds', 'suhu', 'rasa', 'bau', 'ecoli', 'coliform',
        'pH', 'besi', 'flourida', 'kesadahan', 'mangan', 'nitrat', 'nitrit',  'sianida',
        'deterjen', 'pestisida', 'raksa', 'arsen',  'kadmium', 'kromium', 'selenium',
        'seng', 'sulfat', 'timbal', 'benzene', 'organik', 'month', 'created_at', 'updated_at'
    ];

    public function user()
    {
        return $this->belongsTo(\App\User::class);
    }
}
