<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Drinking extends Model
{
    protected $fillable = [
        'user_id', 'location', 'bau', 'warna', 'tds', 'kekeruhan', 'rasa', 'suhu', 'ecoli', 'coliform', 'aluminium',
        'besi', 'kesadahan', 'klorida', 'mangan', 'pH', 'seng', 'sulfat', 'tembaga', 'amonia',
        'arsen', 'flourida', 'kromium', 'kadmium', 'nitrit', 'nitrat', 'sianida', 'selenium', 
        'month', 'created_at', 'updated_at'
    ];

    public function user()
    {
        return $this->belongsTo(\App\User::class);
    }
}
