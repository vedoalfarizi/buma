<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Waste extends Model
{
    protected $fillable = [
        'user_id', 'location', 'pH', 'bod', 'cod', 'tss', 'oil',
        'amoniak', 'coliform', 'month', 'created_at', 'updated_at'
    ];

    public function user()
    {
        return $this->belongsTo(\App\User::class);
    }
}
