<?php

namespace App\Http\Controllers;

use App\Clean;
use App\Drinking;
use App\Waste;
use Illuminate\Support\Facades\Input;

class ChartController extends Controller
{
    private $data, $rules, $dataProcess, $testResult, $charts;
    private $locations = [];

    private function codeToValue(){
        $this->data->map(function ($datum){
            if(isset($datum['bau']) && isset($datum['rasa'])){
                if($datum['bau'] == 0){
                    $datum['bau'] = 'tidak berbau';
                }else{
                    $datum['bau'] = 'berbau';
                }

                if($datum['rasa'] == 0){
                    $datum['rasa'] = 'tidak berasa';
                }else{
                    $datum['rasa'] = 'berasa';
                }
            }
        });
    }

    private function dataProcess(){
        foreach ($this->dataProcess as $preKey => $data){
            foreach ($data as $key => $value){
                if(isset($data['suhu']) && isset($data['bau']) && isset($data['rasa'])){
                    if($key != 'id' && $key != 'user_id' && $key != 'location' && $key != 'suhu' && $key != 'month' && $key != 'comment'
                        && $key != 'created_at' && $key != 'updated_at'){
                        if($key == 'bau' && $key == 'rasa'){
                            if($data[$key] == 0){
                                $this->testResult[$preKey][$key] = 's';
                            }else{
                                $this->testResult[$preKey][$key] = 'ts';
                            }
                        }elseif($key == 'pH'){
                            if($data[$key] >= $this->rules[$key] && $data[$key] <= $this->rules[$key]+2){
                                $this->testResult[$preKey][$key] = 's';
                            }else{
                                $this->testResult[$preKey][$key] = 'ts';
                            }
                        }else{
                            if($data[$key] <= $this->rules[$key]){
                                $this->testResult[$preKey][$key] = 's';
                            }else{
                                $this->testResult[$preKey][$key] = 'ts';
                            }
                        }
                    }
                }else{
                    if($key != 'id' && $key != 'user_id' && $key != 'location' && $key != 'month' && $key != 'comment'
                        && $key != 'created_at' && $key != 'updated_at'){
                        if($key == 'pH'){
                            if($data[$key] >= $this->rules[$key] && $data[$key] <= $this->rules[$key]+2){
                                $this->testResult[$preKey][$key] = 's';
                            }else{
                                $this->testResult[$preKey][$key] = 'ts';
                            }
                        }else{
                            if($data[$key] <= $this->rules[$key]){
                                $this->testResult[$preKey][$key] = 's';
                            }else{
                                $this->testResult[$preKey][$key] = 'ts';
                            }
                        }
                    }
                }
            }
            $this->testResult[$preKey]['month'] = $data['month'];
        }
    }

    private function calcChartValue(){
        $nRules = count($this->rules);

        foreach ($this->testResult as $key => $result){
            $passed = 0;
            foreach ($result as $r){
                if($r == 's'){
                    $passed++;
                }
            }
            $this->charts[$key]['month']  = $this->testResult[$key]['month'];
            $this->charts[$key]['passed'] = number_format($passed / $nRules * 100);
        }
    }

    private function typeFilter($year, $type, $location){
        if($type == 'drink'){
            if($location == null){
                $this->data = Drinking::whereYear('created_at', $year)->get();
            }else{
                $this->data = Drinking::whereYear('created_at', $year)->whereLocation($location)->get();
            }

            $this->dataProcess = $this->data->toArray();

            $this->rules = array(
                'bau' => 0,
                'warna' => 15,
                'tds' => 500,
                'kekeruhan' => 5,
                'rasa' => 0,
                'ecoli' => 0,
                'coliform' => 0,
                'aluminium' => 0.2,
                'besi' => 0.3,
                'kesadahan' => 500,
                'klorida' => 250,
                'mangan' => 0.4,
                'pH' => 6.5,
                'seng' => 3,
                'sulfat' => 250,
                'tembaga' => 2,
                'amonia' => 1.5,
                'arsen' => 0.01,
                'flourida' => 1.5,
                'kromium' => 0.02,
                'kadmium' => 0.003,
                'nitrit' => 0.02,
                'nitrat' => 0.3,
                'sianida' => 0.005,
                'selenium' => 0.001
            );

            $locations = Drinking::select('location')->distinct()->get();

            foreach ($locations as $loc){
                if(!in_array($loc->location, $this->locations)){
                    $this->locations[$loc->location] = $loc->location;
                }
            }
        }elseif($type == 'clean'){
            if($location == null){
                $this->data = Clean::whereYear('created_at', $year)->get();
            }else{
                $this->data = Clean::whereYear('created_at', $year)->whereLocation($location)->get();
            }

            $this->dataProcess = $this->data->toArray();

            $this->rules = array(
                'kekeruhan' => 25,
                'warna' => 50,
                'tds' => 1000,
                'rasa' => 0,
                'bau' => 0,
                'ecoli' => 0,
                'coliform' => 50,
                'pH' => 6.5,
                'besi' => 1,
                'flourida' => 1.5,
                'kesadahan' => 500,
                'mangan' => 0.5,
                'nitrat' => 10,
                'nitrit' => 1,
                'sianida' => 0.1,
                'deterjen' => 0.05,
                'pestisida' => 0.1,
                'raksa' => 0.001,
                'arsen' => 0.05,
                'kadmium' => 0.005,
                'kromium' => 0.05,
                'selenium' => 0.01,
                'seng' => 15,
                'sulfat' => 400,
                'timbal' => 0.05,
                'benzene' => 0.01,
                'organik' => 10,
            );

            $locations = Clean::select('location')->distinct()->get();

            foreach ($locations as $loc){
                if(!in_array($loc->location, $this->locations)){
                    $this->locations[$loc->location] = $loc->location;
                }
            }

        }elseif($type == 'waste'){
            if($location == null){
                $this->data = Waste::whereYear('created_at', $year)->get();
            }else{
                $this->data = Waste::whereYear('created_at', $year)->whereLocation($location)->get();
            }
            $this->dataProcess = $this->data->toArray();

            $this->rules = array(
                'pH' => 6,
                'bod' => 30,
                'cod' => 100,
                'tss' => 30,
                'oil' => 5,
                'amoniak' => 10,
                'coliform' => 3000,
            );

            $locations = Waste::select('location')->distinct()->get();

            foreach ($locations as $loc){
                if(!in_array($loc->location, $this->locations)){
                    $this->locations[$loc->location] = $loc->location;
                }
            }
        }
    }

    public function generateChart(){

        $year = Input::get('year');
        $type = Input::get('type');
        $location = Input::get('location');

        $this->typeFilter($year, $type, $location);

        if($this->data->isEmpty()){
            return "<script>alert('Data untuk Tahun yang dipilih tidak ada.');history.go(-1);</script>";
        }

        $this->codeToValue();
        $this->dataProcess();
        $this->calcChartValue();

        $charts = $this->charts;

        $labels = array();
        $results = array();
        foreach ($charts as $chart){
            array_push($labels, $chart['month']);
            array_push($results, $chart['passed']);
        }

        if($location == null){
            $location = 'Semua Lokasi';
        }

        $locations = $this->locations;

        return view('chart', compact('labels', 'results', 'year', 'type', 'location', 'locations'));
    }


}
