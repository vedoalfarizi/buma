<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Clean;
use Illuminate\Support\Facades\Auth;

class CleanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $cleans = Clean::all();
        $testData = $cleans->toArray();

        $cleans->map(function ($clean){
            if($clean['bau'] == 0){
                $clean['bau'] = 'tidak berbau';
            }else{
                $clean['bau'] = 'berbau';
            }

            if($clean['rasa'] == 0){
                $clean['rasa'] = 'tidak berasa';
            }else{
                $clean['rasa'] = 'berasa';
            }
        });

        $rules = array(
            'kekeruhan' => 25,
            'warna' => 50,
            'tds' => 1000,
            'rasa' => 0,
            'bau' => 0,
            'ecoli' => 0,
            'coliform' => 50,
            'pH' => 6.5,
            'besi' => 1,
            'flourida' => 1.5,
            'kesadahan' => 500,
            'mangan' => 0.5,
            'nitrat' => 10,
            'nitrit' => 1,
            'sianida' => 0.1,
            'deterjen' => 0.05,
            'pestisida' => 0.1,
            'raksa' => 0.001,
            'arsen' => 0.05,
            'kadmium' => 0.005,
            'kromium' => 0.05,
            'selenium' => 0.01,
            'seng' => 15,
            'sulfat' => 400,
            'timbal' => 0.05,
            'benzene' => 0.01,
            'organik' => 10,
        );

        $exception = array(
            'id', 'user_id', 'location', 'suhu', 'month', 'comment', 'created_at', 'updated_at'
        );

        $testResult = array();
        foreach ($testData as $preKey => $clean){
            $nComment = 0;
            foreach ($clean as $key => $c){
                if(!in_array($key, $exception)){
                    if($key == 'bau' && $key == 'rasa'){
                        if($clean[$key] == 0){
                            $testResult[$preKey][$key] = 's';
                        }else{
                            $testResult[$preKey][$key] = 'ts';
                            $nComment++;
                        }
                    }elseif($key == 'pH'){
                        if($clean[$key] >= $rules[$key] && $clean[$key] <= $rules[$key]+2){
                            $testResult[$preKey][$key] = 's';
                        }else{
                            $testResult[$preKey][$key] = 'ts';
                            $nComment++;
                        }
                    }else{
                        if($clean[$key] <= $rules[$key]){
                            $testResult[$preKey][$key] = 's';
                        }else{
                            $testResult[$preKey][$key] = 'ts';
                            $nComment++;
                        }
                    }
                }
            }

            if($nComment > 0){
                $testResult[$preKey]['comment'] = true;
            }else{
                $testResult[$preKey]['comment'] = false;
            }
        }

        return view('clean.index', compact('cleans', 'testResult'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('clean.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = $request->all();
        $input['user_id'] = Auth::user()->id;
        $input['created_at'] = date('Y-m-d H:i:s');

        Clean::create($input);

        return redirect(route('admin.clean.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return null;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $clean = Clean::findOrFail($id);

        return view('clean.edit', compact('clean'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $input = $request->all();
        $input['user_id'] = Auth::user()->id;
        $input['updated_at'] = date('Y-m-d H:i:s');

        $clean = Clean::findOrFail($id);
        $clean->update($input);

        return redirect(route('admin.clean.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $clean = Clean::findOrFail($id);
        $clean->delete();

        return redirect(route('admin.clean.index'));
    }

    public function addComment(Request $request, $id){
        $input = request()->only('comment');

        $clean = Clean::findOrFail($id);
        $clean->comment = $input['comment'];
        $clean->save();

        return redirect(route('admin.clean.index'));
    }
}
