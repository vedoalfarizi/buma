<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Drinking;
use Illuminate\Support\Facades\Auth;

class DrinkingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $drinkings = Drinking::all();
        $testData = $drinkings->toArray();

        $drinkings->map(function ($drinking){
           if($drinking['bau'] == 0){
               $drinking['bau'] = 'tidak berbau';
           }else{
               $drinking['bau'] = 'berbau';
           }

            if($drinking['rasa'] == 0){
                $drinking['rasa'] = 'tidak berasa';
            }else{
                $drinking['rasa'] = 'berasa';
            }
        });

        $rules = array(
            'bau' => 0,
            'warna' => 15,
            'tds' => 500,
            'kekeruhan' => 5,
            'rasa' => 0,
            'ecoli' => 0,
            'coliform' => 0,
            'aluminium' => 0.2,
            'besi' => 0.3,
            'kesadahan' => 500,
            'klorida' => 250,
            'mangan' => 0.4,
            'pH' => 6.5,
            'seng' => 3,
            'sulfat' => 250,
            'tembaga' => 2,
            'amonia' => 1.5,
            'arsen' => 0.01,
            'flourida' => 1.5,
            'kromium' => 0.02,
            'kadmium' => 0.003,
            'nitrit' => 0.02,
            'nitrat' => 0.3,
            'sianida' => 0.005,
            'selenium' => 0.001
        );

        $exception = array(
            'id', 'user_id', 'location', 'suhu', 'month', 'comment', 'created_at', 'updated_at'
        );

        $testResult = array();
        foreach ($testData as $preKey => $drinking){
            $nComment = 0;
            foreach ($drinking as $key => $drink){
                if(!in_array($key, $exception)){
                    if($key == 'bau' && $key == 'rasa'){
                        if($drinking[$key] == 0){
                            $testResult[$preKey][$key] = 's';
                        }else{
                            $testResult[$preKey][$key] = 'ts';
                            $nComment++;
                        }
                    }elseif($key == 'pH'){
                        if($drinking[$key] >= $rules[$key] && $drinking[$key] <= $rules[$key]+2){
                            $testResult[$preKey][$key] = 's';
                        }else{
                            $testResult[$preKey][$key] = 'ts';
                            $nComment++;
                        }
                    }else{
                        if($drinking[$key] <= $rules[$key]){
                            $testResult[$preKey][$key] = 's';
                        }else{
                            $testResult[$preKey][$key] = 'ts';
                            $nComment++;
                        }
                    }
                }
            }

            if($nComment > 0){
                $testResult[$preKey]['comment'] = true;
            }else{
                $testResult[$preKey]['comment'] = false;
            }
        }

        return view('drinking.index', compact('drinkings', 'testResult'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('drinking.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = $request->all();
        $input['user_id'] = Auth::user()->id;
        $input['created_at'] = date('Y-m-d H:i:s');

        Drinking::create($input);

        return redirect(route('admin.drinking.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return null;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $drinking = Drinking::findOrFail($id);

        return view('drinking.edit', compact('drinking'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $input = $request->all();
        $input['user_id'] = Auth::user()->id;
        $input['updated_at'] = date('Y-m-d H:i:s');

        $drinking = Drinking::findOrFail($id);
        $drinking->update($input);

        return redirect(route('admin.drinking.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $drinking = Drinking::findOrFail($id);
        $drinking->delete();

        return redirect(route('admin.drinking.index'));
    }

    public function addComment(Request $request, $id){
        $input = request()->only('comment');

        $drinking = Drinking::findOrFail($id);
        $drinking->comment = $input['comment'];
        $drinking->save();

        return redirect(route('admin.drinking.index'));
    }
}
