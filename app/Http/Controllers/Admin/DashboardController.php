<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Drinking;
use App\Waste;
use App\Clean;

class DashboardController extends Controller
{
    public function index(){
        $drinking   = Drinking::all()->count();
        $waste      = Waste::all()->count();
        $clean      = Clean::all()->count();

        return view('admin.dashboard', compact( 'drinking', 'waste', 'clean'));
    }
}
