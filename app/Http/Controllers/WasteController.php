<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Waste;
use Illuminate\Support\Facades\Auth;

class WasteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $wastes = Waste::all();
        $testData = $wastes->toArray();

        $rules = array(
            'pH' => 6,
            'bod' => 30,
            'cod' => 100,
            'tss' => 30,
            'oil' => 5,
            'amoniak' => 10,
            'coliform' => 3000,
        );

        $exception = array(
            'id', 'user_id', 'location', 'month', 'comment', 'created_at', 'updated_at'
        );

        $testResult = array();
        foreach ($testData as $preKey => $waste){
            $nComment = 0;
            foreach ($waste as $key => $w){
                if(!in_array($key, $exception)){
                    if($key == 'pH'){
                        if($waste[$key] >= $rules[$key] && $waste[$key] <= $rules[$key]+3){
                            $testResult[$preKey][$key] = 's';
                        }else{
                            $testResult[$preKey][$key] = 'ts';
                            $nComment++;
                        }
                    }else{
                        if($waste[$key] <= $rules[$key]){
                            $testResult[$preKey][$key] = 's';
                        }else{
                            $testResult[$preKey][$key] = 'ts';
                            $nComment++;
                        }
                    }
                }
            }

            if($nComment > 0){
                $testResult[$preKey]['comment'] = true;
            }else{
                $testResult[$preKey]['comment'] = false;
            }
        }

        return view('waste.index', compact('wastes', 'testResult'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('waste.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = $request->all();
        $input['user_id'] = Auth::user()->id;
        $input['created_at'] = date('Y-m-d H:i:s');

        Waste::create($input);

        return redirect(route('admin.waste.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return null;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $waste = Waste::findOrFail($id);

        return view('waste.edit', compact('waste'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $input = $request->all();
        $input['user_id'] = Auth::user()->id;
        $input['updated_at'] = date('Y-m-d H:i:s');

        $waste = Waste::findOrFail($id);
        $waste->update($input);

        return redirect(route('admin.waste.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $waste = Waste::findOrFail($id);
        $waste->delete();

        return redirect(route('admin.waste.index'));
    }

    public function addComment(Request $request, $id){
        $input = request()->only('comment');

        $waste = Waste::findOrFail($id);
        $waste->comment = $input['comment'];
        $waste->save();

        return redirect(route('admin.waste.index'));
    }
}
