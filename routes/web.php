<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect(route('login'));
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::group(['middleware' => ['auth', 'admin'], 'prefix' => 'admin', 'as' => 'admin.'], function () {
    Route::get('/dashboard', 'Admin\DashboardController@index')->name('dashboard');
    Route::resource('drinking', 'DrinkingController');
    Route::patch('drinking/comment/{drinking}', 'DrinkingController@addComment')->name('drinking.addComment');
    Route::get('chart/drink', 'ChartController@generateChart')->name('chart.drink');
    Route::resource('waste', 'WasteController');
    Route::patch('waste/comment/{waste}', 'WasteController@addComment')->name('waste.addComment');
    Route::get('chart/waste', 'ChartController@generateChart')->name('chart.waste');
    Route::resource('clean', 'CleanController');
    Route::patch('clean/comment/{clean}', 'WasteController@addComment')->name('clean.addComment');
    Route::get('chart/clean', 'ChartController@generateChart')->name('chart.clean');
});

Route::group(['middleware' => ['auth', 'operator']], function () {
    Route::get('/dashboard', 'DashboardController@index')->name('dashboard');
    
});
