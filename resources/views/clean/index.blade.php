@extends('layouts.app')

@section('content')
  <div class="header bg-gradient-primary pb-6 pt-5 pt-md-8"></div>
  <div class="container-fluid mt--7">
    <div class="row">
      <div class="col">
        <div class="card shadow">
          <div class="card-header border-0">
            <div class="row align-items-center">
              <div class="col">
                <h3 class="mb-0">Tabel Air Bersih</h3>
              </div>
              <div class="col text-right">
                <a href="{{route('admin.clean.create')}}" class="btn btn-sm btn-primary">Tambah data</a>
              </div>
            </div>
          </div>
          <div class="table-responsive">
            <table class="table align-items-center table-flush">
              <thead class="thead-light">
                <tr>
                  <th>No</th>
                  <th scope="col">Operator</th>
                  <th scope="col">Location</th>
                  <th scope="col">Month</th>
                  <th scope="col">Comment</th>
                  <th scope="col">Kekeruhan</th>
                  <th scope="col">Warna</th>
                  <th scope="col">TDS</th>
                  <th scope="col">Suhu</th>
                  <th scope="col">Rasa</th>
                  <th scope="col">Bau</th>
                  <th scope="col">E-Coli</th>
                  <th scope="col">Coliform</th>
                  <th scope="col">pH</th>
                  <th scope="col">Besi</th>
                  <th scope="col">Flourida</th>
                  <th scope="col">Kesadahan</th>
                  <th scope="col">Mangan</th>
                  <th scope="col">Nitrat</th>
                  <th scope="col">Nitrit</th>
                  <th scope="col">Sianida</th>
                  <th scope="col">Deterjen</th>
                  <th scope="col">Pestisida</th>
                  <th scope="col">Air Raksa</th>
                  <th scope="col">Arsen</th>
                  <th scope="col">Kadmium</th>
                  <th scope="col">Kromium</th>
                  <th scope="col">Selenium</th>
                  <th scope="col">Seng</th>
                  <th scope="col">Sulfat</th>
                  <th scope="col">Timbal</th>
                  <th scope="col">Benzene</th>
                  <th scope="col">Zat Organik</th>
                  <th scope="col">Created At</th>
                  <th scope="col">Updated At</th>
                  <th class="text-right" scope="col">Action</th>
                </tr>
              </thead>
              <tbody>
                @php
                  $n = 1;
                @endphp
                @foreach ($cleans as $key => $clean)
                  <tr>     
                    <td>{{$n++}}</td>               
                    <td>{{$clean->user->name}}</td>
                    <td>{{$clean->location}}</td>
                    <td>{{$clean->month}}</td>
                    <td>@if($testResult[$key]['comment'] == true)
                        @if($clean->comment == null)
                          <button type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#inputComment-{{$clean->id}}">Tambah Komentar</button>
                        @else
                          <button type="button" class="btn btn-success btn-sm" data-toggle="modal" data-target="#showComment-{{$clean->id}}">Lihat Komentar</button>
                        @endif
                      @else
                        Hasil sesuai standar Permenkes
                      @endif
                    </td>
                    <td @if($testResult[$key]['kekeruhan'] == 'ts') style="color: red" @endif>{{$clean->kekeruhan}}</td>
                    <td @if($testResult[$key]['warna'] == 'ts') style="color: red" @endif>{{$clean->warna}}</td>
                    <td @if($testResult[$key]['tds'] == 'ts') style="color: red" @endif>{{$clean->tds}}</td>
                    <td>{{$clean->suhu}}</td>
                    <td @if($testResult[$key]['rasa'] == 'ts') style="color: red" @endif>{{$clean->rasa}}</td>
                    <td @if($testResult[$key]['bau'] == 'ts') style="color: red" @endif>{{$clean->bau}}</td>
                    <td @if($testResult[$key]['ecoli'] == 'ts') style="color: red" @endif>{{$clean->ecoli}}</td>
                    <td @if($testResult[$key]['coliform'] == 'ts') style="color: red" @endif>{{$clean->coliform}}</td>
                    <td @if($testResult[$key]['pH'] == 'ts') style="color: red" @endif>{{$clean->pH}}</td>
                    <td @if($testResult[$key]['besi'] == 'ts') style="color: red" @endif>{{$clean->besi}}</td>
                    <td @if($testResult[$key]['flourida'] == 'ts') style="color: red" @endif>{{$clean->flourida}}</td>
                    <td @if($testResult[$key]['kesadahan'] == 'ts') style="color: red" @endif>{{$clean->kesadahan}}</td>
                    <td @if($testResult[$key]['mangan'] == 'ts') style="color: red" @endif>{{$clean->mangan}}</td>
                    <td @if($testResult[$key]['nitrat'] == 'ts') style="color: red" @endif>{{$clean->nitrat}}</td>
                    <td @if($testResult[$key]['nitrit'] == 'ts') style="color: red" @endif>{{$clean->nitrit}}</td>
                    <td @if($testResult[$key]['sianida'] == 'ts') style="color: red" @endif>{{$clean->sianida}}</td>
                    <td @if($testResult[$key]['deterjen'] == 'ts') style="color: red" @endif>{{$clean->deterjen}}</td>
                    <td @if($testResult[$key]['pestisida'] == 'ts') style="color: red" @endif>{{$clean->pestisida}}</td>
                    <td @if($testResult[$key]['raksa'] == 'ts') style="color: red" @endif>{{$clean->raksa}}</td>
                    <td @if($testResult[$key]['arsen'] == 'ts') style="color: red" @endif>{{$clean->arsen}}</td>
                    <td @if($testResult[$key]['kadmium'] == 'ts') style="color: red" @endif>{{$clean->kadmium}}</td>
                    <td @if($testResult[$key]['kromium'] == 'ts') style="color: red" @endif>{{$clean->kromium}}</td>
                    <td @if($testResult[$key]['selenium'] == 'ts') style="color: red" @endif>{{$clean->selenium}}</td>
                    <td @if($testResult[$key]['seng'] == 'ts') style="color: red" @endif>{{$clean->seng}}</td>
                    <td @if($testResult[$key]['sulfat'] == 'ts') style="color: red" @endif>{{$clean->sulfat}}</td>
                    <td @if($testResult[$key]['timbal'] == 'ts') style="color: red" @endif>{{$clean->timbal}}</td>
                    <td @if($testResult[$key]['benzene'] == 'ts') style="color: red" @endif>{{$clean->benzene}}</td>
                    <td @if($testResult[$key]['organik'] == 'ts') style="color: red" @endif>{{$clean->organik}}</td>
                    <td>{{$clean->created_at->format('d F Y, H:i')}}</td>
                    <td>{{$clean->updated_at->format('d F Y, H:i')}}</td>
                    <td class="text-right">
                        <div class="dropdown">
                        <a class="btn btn-sm btn-icon-only text-light" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <i class="fas fa-ellipsis-v"></i>
                        </a>
                        {!! Form::open(['route' => ['admin.clean.destroy', $clean->id], 'method' => 'delete']) !!}
                        <div class="dropdown-menu dropdown-menu-right dropdown-menu-arrow">
                            <a class="dropdown-item" href="{{route('admin.clean.edit', [$clean->id])}}">Edit</a>
                            {!! Form::button('Delete', ['type' => 'submit', 'class' => 'dropdown-item', 'onclick' => "return confirm('Are you sure to delete this item?')"]) !!}
                        </div>
                        {!! Form::close() !!}
                        </div>
                    </td>
                  </tr>

                  <div id="inputComment-{{$clean->id}}" class="modal fade" role="dialog">
                    <div class="modal-dialog">
                      <div class="modal-content">
                        <div class="modal-header">
                          <button type="button" class="close" data-dismiss="modal">&times;</button>
                        </div>
                        <div class="modal-body">
                          {!! Form::open(['route' => ['admin.clean.addComment', $clean->id], 'method' => 'post']) !!}
                          @method('PATCH')
                          <div class="pl-lg-4">
                            <div class="row">
                              <div class="col-lg-12">
                                <div class="form-group">
                                  {!! Form::textarea('comment', null, ['class' => 'form-control form-control-alternative', 'placeholder' => 'Tulis Komentar']) !!}
                                </div>
                              </div>
                            </div>
                            <div class="row">
                              <div class="col-lg-6">
                                <div class="form-group">
                                  {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
                                </div>
                              </div>
                            </div>
                          </div>
                          {!! Form::close() !!}
                        </div>
                      </div>
                    </div>
                  </div>

                  <div id="showComment-{{$clean->id}}" class="modal fade" role="dialog">
                    <div class="modal-dialog">
                      <div class="modal-content">
                        <div class="modal-header">
                          <button type="button" class="close" data-dismiss="modal">&times;</button>
                        </div>
                        <div class="modal-body">
                          {!! Form::open(['route' => ['admin.clean.addComment', $clean->id], 'method' => 'post']) !!}
                          @method('PATCH')
                          <div class="pl-lg-4">
                            <div class="row">
                              <div class="col-lg-12">
                                <div class="form-group">
                                  {!! Form::textarea('comment', $clean->comment, ['class' => 'form-control form-control-alternative']) !!}
                                </div>
                              </div>
                            </div>
                            <div class="row">
                              <div class="col-lg-6">
                                <div class="form-group">
                                  {!! Form::submit('Update', ['class' => 'btn btn-primary']) !!}
                                </div>
                              </div>
                            </div>
                          </div>
                          {!! Form::close() !!}
                        </div>
                      </div>
                    </div>
                  </div>

                @endforeach
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>

    @include('layouts.footer')
      
  </div>
@endsection