@extends('layouts.app')

@section('content')
  <div class="header bg-gradient-primary pb-6 pt-5 pt-md-8"></div>
  <div class="container-fluid mt--7">
    <div class="row">
      <div class="col">
        <div class="card shadow">
          <div class="card-header border-0">
            <div class="row align-items-center">
              <div class="col">
                <h3 class="mb-0">Edit data</h3>
              </div>
            </div>
          </div>
          <div class="card-body">
            {!! Form::model($clean, ['route' => ['admin.clean.update', $clean->id], 'method' => 'patch']) !!}

              @include('clean.fields')

            {!! Form::close() !!}
          </div>
        </div>
      </div>
    </div>

    @include('layouts.footer')
      
  </div>
@endsection