@extends('layouts.app')

@section('content')
    <div class="header bg-gradient-primary pb-8 pt-5 pt-md-8">
        <div class="container-fluid">
            <div class="row">
                <div class="col">
                    <div class="card shadow">
                        <div class="card-header border-0">
                            <div class="row align-items-center">
                                <div class="col">
                                    <h3 class="mb-0">Filter Tahun</h3>
                                </div>
                            </div>
                        </div>
                        <div class="card-body">
                            {!! Form::open(['route' => 'admin.chart.'.$type.'', 'method' => 'GET']) !!}
                            <div class="pl-lg-4">
                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            {!! Form::selectRange('year', 2015, date('Y'), null, ['class' => 'form-control form-control-alternative', 'placeholder' => '--Pilih Tahun--']) !!}
                                            {!! Form::hidden('type', $type) !!}
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            {!! Form::select('location', $locations, null, ['class' => 'form-control form-control-alternative', 'placeholder' => '--Pilih Lokasi--']) !!}
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        {!! Form::submit('Filter', ['class' => 'btn btn-primary']) !!}
                                    </div>
                                </div>
                            </div>
                        </div>
                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
            </div>
            <br>
            <div class="row">
                <div class="col bg-white">
                    <canvas id="myChart"></canvas>
                </div>
            </div>
        </div>
    </div>

    <script>
        var ctx = document.getElementById("myChart").getContext('2d');
        var myChart = new Chart(ctx, {
            type: 'bar',
            data: {
                labels: {!! json_encode($labels) !!},
                datasets: [{
                    label: "Kualitas {{$type}}(%) Tahun {{$year}} di {{$location}}",
                    data: {!! json_encode($results) !!},
                    borderColor: 'rgba(0, 0, 0, 0.1)',
                    backgroundColor: 'rgba(54, 162, 235, 0.2)',
                    borderWidth: 1
                }]
            },
            options: {
                scales: {
                    yAxes: [{
                        ticks: {
                            beginAtZero:true,
                            max:100
                        }
                    }]
                }
            }
        });
    </script>
@endsection
