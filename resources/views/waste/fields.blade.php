<div class="pl-lg-4">
    <div class="row">
        <div class="col-lg-6">
            <div class="form-group">
                {!! Form::select('month', ['Januari' => 'Januari', 'Februari' => 'Februari', 'Maret' => 'Maret', 'April' => 'April', 'Mei' => 'Mei', 'Juni' => 'Juni', 'Juli' => 'Juli', 'Agustus'=>'Agustus', 'September'=>'September', 'Oktober'=>'Oktober', 'November'=>'November', 'Desember'=>'Desember'],null, ['class' => 'form-control form-control-alternative', 'placeholder' => '--Select Month--']) !!}
            </div>
        </div>
        <div class="col-lg-6">
            <div class="form-group">
                {!! Form::text('location', null, ['class' => 'form-control form-control-alternative', 'placeholder' => 'Location']) !!}
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-3">
            <div class="form-group">
                {!! Form::number('pH', null, ['class' => 'form-control form-control-alternative', 'placeholder' => 'pH', 'step' => 'any']) !!}
            </div>
        </div>
        <div class="col-lg-3">
            <div class="form-group">
                {!! Form::number('bod', null, ['class' => 'form-control form-control-alternative', 'placeholder' => 'BOD', 'step' => 'any']) !!}
            </div>
        </div>
        <div class="col-lg-3">
            <div class="form-group">
                {!! Form::number('cod', null, ['class' => 'form-control form-control-alternative', 'placeholder' => 'COD', 'step' => 'any']) !!}
            </div>
        </div>
        <div class="col-lg-3">
            <div class="form-group">
                {!! Form::number('tss', null, ['class' => 'form-control form-control-alternative', 'placeholder' => 'TSS', 'step' => 'any']) !!}
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-3">
            <div class="form-group">
                {!! Form::number('oil', null, ['class' => 'form-control form-control-alternative', 'placeholder' => 'Oil', 'step' => 'any']) !!}
            </div>
        </div>
        <div class="col-lg-3">
            <div class="form-group">
                {!! Form::number('amoniak', null, ['class' => 'form-control form-control-alternative', 'placeholder' => 'Amoniak', 'step' => 'any']) !!}
            </div>
        </div>
        <div class="col-lg-3">
            <div class="form-group">
                {!! Form::number('coliform', null, ['class' => 'form-control form-control-alternative', 'placeholder' => 'Total Coliform', 'step' => 'any']) !!}
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <div class="form-group">
                {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
                <a class="btn btn-default" href="{{route('admin.waste.index')}}">Cancel</a>
            </div>
        </div>
    </div>
</div>