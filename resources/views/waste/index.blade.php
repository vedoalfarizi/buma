@extends('layouts.app')

@section('content')
    <div class="header bg-gradient-primary pb-6 pt-5 pt-md-8"></div>
    <div class="container-fluid mt--7">
        <div class="row">
            <div class="col">
                <div class="card shadow">
                    <div class="card-header border-0">
                        <div class="row align-items-center">
                            <div class="col">
                                <h3 class="mb-0">Tabel Air Limbah</h3>
                            </div>
                            <div class="col text-right">
                                <a href="{{route('admin.waste.create')}}" class="btn btn-sm btn-primary">Tambah data</a>
                            </div>
                        </div>
                    </div>
                    <div class="table-responsive">
                        <table class="table align-items-center table-flush">
                            <thead class="thead-light">
                            <tr>
                                <th>No</th>
                                <th scope="col">Operator</th>
                                <th scope="col">Location</th>
                                <th scope="col">Month</th>
                                <th scope="col">Comment</th>
                                <th scope="col">pH</th>
                                <th scope="col">BOD</th>
                                <th scope="col">COD</th>
                                <th scope="col">TSS</th>
                                <th scope="col">Oil</th>
                                <th scope="col">Amoniak</th>
                                <th scope="col">Total Coliform</th>
                                <th scope="col">Created At</th>
                                <th scope="col">Updated At</th>
                                <th class="text-right" scope="col">Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            @php
                                $n = 1;
                            @endphp
                            @foreach ($wastes as $key => $waste)
                                <tr>
                                    <td>{{$n++}}</td>
                                    <td>{{$waste->user->name}}</td>
                                    <td>{{$waste->location}}</td>
                                    <td>{{$waste->month}}</td>
                                    <td>@if($testResult[$key]['comment'] == true)
                                            @if($waste->comment == null)
                                                <button type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#inputComment-{{$waste->id}}">Tambah Komentar</button>
                                            @else
                                                <button type="button" class="btn btn-success btn-sm" data-toggle="modal" data-target="#showComment-{{$waste->id}}">Lihat Komentar</button>
                                            @endif
                                        @else
                                            Hasil sesuai standar Permenkes
                                        @endif
                                    </td>
                                    <td @if($testResult[$key]['pH'] == 'ts') style="color: red" @endif>{{$waste->pH}}</td>
                                    <td @if($testResult[$key]['bod'] == 'ts') style="color: red" @endif>{{$waste->bod}}</td>
                                    <td @if($testResult[$key]['cod'] == 'ts') style="color: red" @endif>{{$waste->cod}}</td>
                                    <td @if($testResult[$key]['tss'] == 'ts') style="color: red" @endif>{{$waste->tss}}</td>
                                    <td @if($testResult[$key]['oil'] == 'ts') style="color: red" @endif>{{$waste->oil}}</td>
                                    <td @if($testResult[$key]['amoniak'] == 'ts') style="color: red" @endif>{{$waste->amoniak}}</td>
                                    <td @if($testResult[$key]['coliform'] == 'ts') style="color: red" @endif>{{$waste->coliform}}</td>
                                    <td>{{$waste->created_at->format('d F Y, H:i')}}</td>
                                    <td>{{$waste->updated_at->format('d F Y, H:i')}}</td>
                                    <td class="text-right">
                                        <div class="dropdown">
                                            <a class="btn btn-sm btn-icon-only text-light" href="#" role="button"
                                               data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                <i class="fas fa-ellipsis-v"></i>
                                            </a>
                                            {!! Form::open(['route' => ['admin.waste.destroy', $waste->id], 'method' => 'delete']) !!}
                                            <div class="dropdown-menu dropdown-menu-right dropdown-menu-arrow">
                                                <a class="dropdown-item"
                                                   href="{{route('admin.waste.edit', [$waste->id])}}">Edit</a>
                                                {!! Form::button('Delete', ['type' => 'submit', 'class' => 'dropdown-item', 'onclick' => "return confirm('Are you sure to delete this item?')"]) !!}
                                            </div>
                                            {!! Form::close() !!}
                                        </div>
                                    </td>
                                </tr>

                                <div id="inputComment-{{$waste->id}}" class="modal fade" role="dialog">
                                    <div class="modal-dialog">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                            </div>
                                            <div class="modal-body">
                                                {!! Form::open(['route' => ['admin.waste.addComment', $waste->id], 'method' => 'post']) !!}
                                                @method('PATCH')
                                                <div class="pl-lg-4">
                                                    <div class="row">
                                                        <div class="col-lg-12">
                                                            <div class="form-group">
                                                                {!! Form::textarea('comment', null, ['class' => 'form-control form-control-alternative', 'placeholder' => 'Tulis Komentar']) !!}
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-lg-6">
                                                            <div class="form-group">
                                                                {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                {!! Form::close() !!}
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div id="showComment-{{$waste->id}}" class="modal fade" role="dialog">
                                    <div class="modal-dialog">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                            </div>
                                            <div class="modal-body">
                                                {!! Form::open(['route' => ['admin.waste.addComment', $waste->id], 'method' => 'post']) !!}
                                                @method('PATCH')
                                                <div class="pl-lg-4">
                                                    <div class="row">
                                                        <div class="col-lg-12">
                                                            <div class="form-group">
                                                                {!! Form::textarea('comment', $waste->comment, ['class' => 'form-control form-control-alternative']) !!}
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-lg-6">
                                                            <div class="form-group">
                                                                {!! Form::submit('Update', ['class' => 'btn btn-primary']) !!}
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                {!! Form::close() !!}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>

        @include('layouts.footer')

    </div>
@endsection