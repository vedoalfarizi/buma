@extends('layouts.app')

@section('content')
    <div class="header bg-gradient-primary pb-6 pt-5 pt-md-8"></div>
    <div class="container-fluid mt--7">
        <div class="row">
            <div class="col">
                <div class="card shadow">
                    <div class="card-header border-0">
                        <div class="row align-items-center">
                            <div class="col">
                                <h3 class="mb-0">Tabel Air Minum</h3>
                            </div>
                            <div class="col text-right">
                                <a href="{{route('admin.drinking.create')}}" class="btn btn-sm btn-primary">Tambah
                                    data</a>
                            </div>
                        </div>
                    </div>
                    <div class="table-responsive">
                        <table class="table align-items-center table-flush">
                            <thead class="thead-light">
                            <tr>
                                <th>No</th>
                                <th scope="col">Operator</th>
                                <th scope="col">Location</th>
                                <th scope="col">Month</th>
                                <th scope="col">Comment</th>
                                <th scope="col">Bau</th>
                                <th scope="col">Warna</th>
                                <th scope="col">TDS</th>
                                <th scope="col">Kekeruhan</th>
                                <th scope="col">Rasa</th>
                                <th scope="col">Suhu</th>
                                <th scope="col">E-Coli</th>
                                <th scope="col">Coliform</th>
                                <th scope="col">Aluminium</th>
                                <th scope="col">Besi</th>
                                <th scope="col">Kesadahan</th>
                                <th scope="col">Klorida</th>
                                <th scope="col">Mangan</th>
                                <th scope="col">pH</th>
                                <th scope="col">Seng</th>
                                <th scope="col">Sulfat</th>
                                <th scope="col">Tembaga</th>
                                <th scope="col">Amonia</th>
                                <th scope="col">Arsen</th>
                                <th scope="col">Flourida</th>
                                <th scope="col">Kromium</th>
                                <th scope="col">Kadmium</th>
                                <th scope="col">Nitrit</th>
                                <th scope="col">Nitrat</th>
                                <th scope="col">Sianida</th>
                                <th scope="col">Selenium</th>
                                <th scope="col">Created At</th>
                                <th scope="col">Updated At</th>
                                <th class="text-right" scope="col">Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            @php
                                $n = 1;
                            @endphp
                            @foreach ($drinkings as $key => $drinking)
                                <tr>
                                    <td>{{$n++}}</td>
                                    <td>{{$drinking->user->name}}</td>
                                    <td>{{$drinking->location}}</td>
                                    <td>{{$drinking->month}}</td>
                                    <td>@if($testResult[$key]['comment'] == true)
                                            @if($drinking->comment == null)
                                                <button type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#inputComment-{{$drinking->id}}">Tambah Komentar</button>
                                            @else
                                                <button type="button" class="btn btn-success btn-sm" data-toggle="modal" data-target="#showComment-{{$drinking->id}}">Lihat Komentar</button>
                                            @endif
                                        @else
                                            Hasil sesuai standar Permenkes
                                        @endif
                                    </td>
                                    <td @if($testResult[$key]['bau'] == 'ts') style="color: red" @endif>{{$drinking->bau}}</td>
                                    <td @if($testResult[$key]['warna'] == 'ts') style="color: red" @endif>{{$drinking->warna}}</td>
                                    <td @if($testResult[$key]['tds'] == 'ts') style="color: red" @endif>{{$drinking->tds}}</td>
                                    <td @if($testResult[$key]['kekeruhan'] == 'ts') style="color: red" @endif>{{$drinking->kekeruhan}}</td>
                                    <td @if($testResult[$key]['rasa'] == 'ts') style="color: red" @endif>{{$drinking->rasa}}</td>
                                    <td>{{$drinking->suhu}}</td>
                                    <td @if($testResult[$key]['ecoli'] == 'ts') style="color: red" @endif>{{$drinking->ecoli}}</td>
                                    <td @if($testResult[$key]['coliform'] == 'ts') style="color: red" @endif>{{$drinking->coliform}}</td>
                                    <td @if($testResult[$key]['aluminium'] == 'ts') style="color: red" @endif>{{$drinking->aluminium}}</td>
                                    <td @if($testResult[$key]['besi'] == 'ts') style="color: red" @endif>{{$drinking->besi}}</td>
                                    <td @if($testResult[$key]['kesadahan'] == 'ts') style="color: red" @endif>{{$drinking->kesadahan}}</td>
                                    <td @if($testResult[$key]['klorida'] == 'ts') style="color: red" @endif>{{$drinking->klorida}}</td>
                                    <td @if($testResult[$key]['mangan'] == 'ts') style="color: red" @endif>{{$drinking->mangan}}</td>
                                    <td @if($testResult[$key]['pH'] == 'ts') style="color: red" @endif>{{$drinking->pH}}</td>
                                    <td @if($testResult[$key]['seng'] == 'ts') style="color: red" @endif>{{$drinking->seng}}</td>
                                    <td @if($testResult[$key]['sulfat'] == 'ts') style="color: red" @endif>{{$drinking->sulfat}}</td>
                                    <td @if($testResult[$key]['tembaga'] == 'ts') style="color: red" @endif>{{$drinking->tembaga}}</td>
                                    <td @if($testResult[$key]['amonia'] == 'ts') style="color: red" @endif>{{$drinking->amonia}}</td>
                                    <td @if($testResult[$key]['arsen'] == 'ts') style="color: red" @endif>{{$drinking->arsen}}</td>
                                    <td @if($testResult[$key]['flourida'] == 'ts') style="color: red" @endif>{{$drinking->flourida}}</td>
                                    <td @if($testResult[$key]['kromium'] == 'ts') style="color: red" @endif>{{$drinking->kromium}}</td>
                                    <td @if($testResult[$key]['kadmium'] == 'ts') style="color: red" @endif>{{$drinking->kadmium}}</td>
                                    <td @if($testResult[$key]['nitrit'] == 'ts') style="color: red" @endif>{{$drinking->nitrit}}</td>
                                    <td @if($testResult[$key]['nitrat'] == 'ts') style="color: red" @endif>{{$drinking->nitrat}}</td>
                                    <td @if($testResult[$key]['sianida'] == 'ts') style="color: red" @endif>{{$drinking->sianida}}</td>
                                    <td @if($testResult[$key]['selenium'] == 'ts') style="color: red" @endif>{{$drinking->selenium}}</td>
                                    <td>{{$drinking->created_at->format('d F Y, H:i')}}</td>
                                    <td>{{$drinking->updated_at->format('d F Y, H:i')}}</td>
                                    <td class="text-right">
                                        <div class="dropdown">
                                            <a class="btn btn-sm btn-icon-only text-light" href="#" role="button"
                                               data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                <i class="fas fa-ellipsis-v"></i>
                                            </a>
                                            {!! Form::open(['route' => ['admin.drinking.destroy', $drinking->id], 'method' => 'delete']) !!}
                                            <div class="dropdown-menu dropdown-menu-right dropdown-menu-arrow">
                                                <a class="dropdown-item"
                                                   href="{{route('admin.drinking.edit', [$drinking->id])}}">Edit</a>
                                                {!! Form::button('Delete', ['type' => 'submit', 'class' => 'dropdown-item', 'onclick' => "return confirm('Are you sure to delete this item?')"]) !!}
                                            </div>
                                            {!! Form::close() !!}
                                        </div>
                                    </td>
                                </tr>

                                <div id="inputComment-{{$drinking->id}}" class="modal fade" role="dialog">
                                    <div class="modal-dialog">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                            </div>
                                            <div class="modal-body">
                                                {!! Form::open(['route' => ['admin.drinking.addComment', $drinking->id], 'method' => 'post']) !!}
                                                    @method('PATCH')
                                                <div class="pl-lg-4">
                                                    <div class="row">
                                                        <div class="col-lg-12">
                                                            <div class="form-group">
                                                                {!! Form::textarea('comment', null, ['class' => 'form-control form-control-alternative', 'placeholder' => 'Tulis Komentar']) !!}
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-lg-6">
                                                            <div class="form-group">
                                                                {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                {!! Form::close() !!}
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div id="showComment-{{$drinking->id}}" class="modal fade" role="dialog">
                                    <div class="modal-dialog">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                            </div>
                                            <div class="modal-body">
                                                {!! Form::open(['route' => ['admin.drinking.addComment', $drinking->id], 'method' => 'post']) !!}
                                                @method('PATCH')
                                                <div class="pl-lg-4">
                                                    <div class="row">
                                                        <div class="col-lg-12">
                                                            <div class="form-group">
                                                                {!! Form::textarea('comment', $drinking->comment, ['class' => 'form-control form-control-alternative']) !!}
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-lg-6">
                                                            <div class="form-group">
                                                                {!! Form::submit('Update', ['class' => 'btn btn-primary']) !!}
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                {!! Form::close() !!}
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>

        @include('layouts.footer')

    </div>
@endsection