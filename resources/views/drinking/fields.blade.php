<div class="pl-lg-4">
    <div class="row">
        <div class="col-lg-6">
            <div class="form-group">
                {!! Form::select('month', ['Januari' => 'Januari', 'Februari' => 'Februari', 'Maret' => 'Maret', 'April' => 'April', 'Mei' => 'Mei', 'Juni' => 'Juni', 'Juli' => 'Juli', 'Agustus'=>'Agustus', 'September'=>'September', 'Oktober'=>'Oktober', 'November'=>'November', 'Desember'=>'Desember'],null, ['class' => 'form-control form-control-alternative', 'placeholder' => '--Select Month--']) !!}
            </div>
        </div>
        <div class="col-lg-6">
            <div class="form-group">
                {!! Form::text('location', null, ['class' => 'form-control form-control-alternative', 'placeholder' => 'Location']) !!}
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-3">
            <div class="form-group">
                {!! Form::select('bau', [0 => 'tidak berbau', 1 => 'berbau'], null, ['class' => 'form-control form-control-alternative', 'placeholder' => 'Pilih']) !!}
            </div>
        </div>
        <div class="col-lg-3">
            <div class="form-group">
                {!! Form::number('warna', null, ['class' => 'form-control form-control-alternative', 'placeholder' => 'Warna', 'step' => 'any']) !!}
            </div>
        </div>
        <div class="col-lg-3">
            <div class="form-group">
                {!! Form::number('tds', null, ['class' => 'form-control form-control-alternative', 'placeholder' => 'TDS', 'step' => 'any']) !!}
            </div>
        </div>
        <div class="col-lg-3">
            <div class="form-group">
                {!! Form::number('kekeruhan', null, ['class' => 'form-control form-control-alternative', 'placeholder' => 'Kekeruhan', 'step' => 'any']) !!}
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-3">
            <div class="form-group">
                {!! Form::select('rasa', [0 => 'tidak berasa', 1 => 'berasa'], null, ['class' => 'form-control form-control-alternative', 'placeholder' => 'Pilih']) !!}
            </div>
        </div>
        <div class="col-lg-3">
            <div class="form-group">
                {!! Form::number('suhu', null, ['class' => 'form-control form-control-alternative', 'placeholder' => 'Suhu', 'step' => 'any']) !!}
            </div>
        </div>
        <div class="col-lg-3">
            <div class="form-group">
                {!! Form::number('ecoli', null, ['class' => 'form-control form-control-alternative', 'placeholder' => 'E-Coli', 'step' => 'any']) !!}
            </div>
        </div>
        <div class="col-lg-3">
            <div class="form-group">
                {!! Form::number('coliform', null, ['class' => 'form-control form-control-alternative', 'placeholder' => 'Coliform', 'step' => 'any']) !!}
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-3">
            <div class="form-group">
                {!! Form::number('aluminium', null, ['class' => 'form-control form-control-alternative', 'placeholder' => 'Aluminium', 'step' => 'any']) !!}
            </div>
        </div>
        <div class="col-lg-3">
            <div class="form-group">
                {!! Form::number('besi', null, ['class' => 'form-control form-control-alternative', 'placeholder' => 'Besi', 'step' => 'any']) !!}
            </div>
        </div>
        <div class="col-lg-3">
            <div class="form-group">
                {!! Form::number('kesadahan', null, ['class' => 'form-control form-control-alternative', 'placeholder' => 'Kesadahan', 'step' => 'any']) !!}
            </div>
        </div>
        <div class="col-lg-3">
            <div class="form-group">
                {!! Form::number('klorida', null, ['class' => 'form-control form-control-alternative', 'placeholder' => 'Klorida', 'step' => 'any']) !!}
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-3">
            <div class="form-group">
                {!! Form::number('mangan', null, ['class' => 'form-control form-control-alternative', 'placeholder' => 'Mangan', 'step' => 'any']) !!}
            </div>
        </div>
        <div class="col-lg-3">
            <div class="form-group">
                {!! Form::number('pH', null, ['class' => 'form-control form-control-alternative', 'placeholder' => 'pH', 'step' => 'any']) !!}
            </div>
        </div>
        <div class="col-lg-3">
            <div class="form-group">
                {!! Form::number('seng', null, ['class' => 'form-control form-control-alternative', 'placeholder' => 'Seng', 'step' => 'any']) !!}
            </div>
        </div>
        <div class="col-lg-3">
            <div class="form-group">
                {!! Form::number('sulfat', null, ['class' => 'form-control form-control-alternative', 'placeholder' => 'Sulfat', 'step' => 'any']) !!}
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-3">
            <div class="form-group">
                {!! Form::number('tembaga', null, ['class' => 'form-control form-control-alternative', 'placeholder' => 'Tembaga', 'step' => 'any']) !!}
            </div>
        </div>
        <div class="col-lg-3">
            <div class="form-group">
                {!! Form::number('amonia', null, ['class' => 'form-control form-control-alternative', 'placeholder' => 'Amonia', 'step' => 'any']) !!}
            </div>
        </div>
        <div class="col-lg-3">
            <div class="form-group">
                {!! Form::number('arsen', null, ['class' => 'form-control form-control-alternative', 'placeholder' => 'Arsen', 'step' => 'any']) !!}
            </div>
        </div>
        <div class="col-lg-3">
            <div class="form-group">
                {!! Form::number('flourida', null, ['class' => 'form-control form-control-alternative', 'placeholder' => 'Flourida', 'step' => 'any']) !!}
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-3">
            <div class="form-group">
                {!! Form::number('kromium', null, ['class' => 'form-control form-control-alternative', 'placeholder' => 'Kromium', 'step' => 'any']) !!}
            </div>
        </div>
        <div class="col-lg-3">
            <div class="form-group">
                {!! Form::number('kadmium', null, ['class' => 'form-control form-control-alternative', 'placeholder' => 'Kadmium', 'step' => 'any']) !!}
            </div>
        </div>
        <div class="col-lg-3">
            <div class="form-group">
                {!! Form::number('nitrit', null, ['class' => 'form-control form-control-alternative', 'placeholder' => 'Nitrit', 'step' => 'any']) !!}
            </div>
        </div>
        <div class="col-lg-3">
            <div class="form-group">
                {!! Form::number('nitrat', null, ['class' => 'form-control form-control-alternative', 'placeholder' => 'Nitrat', 'step' => 'any']) !!}
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-3">
            <div class="form-group">
                {!! Form::number('sianida', null, ['class' => 'form-control form-control-alternative', 'placeholder' => 'Sianida', 'step' => 'any']) !!}
            </div>
        </div>
        <div class="col-lg-3">
            <div class="form-group">
                {!! Form::number('selenium', null, ['class' => 'form-control form-control-alternative', 'placeholder' => 'Selenium', 'step' => 'any']) !!}
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <div class="form-group">
                {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
                <a class="btn btn-default" href="{{route('admin.drinking.index')}}">Cancel</a>
            </div>
        </div>
    </div>
</div>