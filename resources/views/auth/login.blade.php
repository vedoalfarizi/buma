<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="Sistem Informasi Manajemen Bantuan Dana Kegiatan Kemahasiswaan">
    <meta name="author" content="vedo alfarizi">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <link href="{{asset('img/favicon.png')}}" rel="icon" type="image/png">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet">
    <link href="{{asset('css/nucleo.css')}}" rel="stylesheet">
    <link href="{{asset('css/all.min.css')}}" rel="stylesheet">
    <link type="text/css" href="{{asset('css/argon.min.css')}}" rel="stylesheet">

</head>
<body class="bg-default">
    <div class="main-content">
        <div class="header d-flex align-items-center py-9 py-lg-6" style="background-image: url('{{asset('img/favicon.png')}}'); background-size: cover; background-position: center;">
            <span class="mask bg-gradient-default opacity-4"></span>
            <div class="container">
                <div class="header-body text-center mb-7">
                    <div class="row justify-content-center">
                    <div class="col-lg-7 col-md-8">
                        <h1 class="text-white">WQC</h1>
                        <p class="text-lead text-light">Water Quality Control</p>
                    </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Page content -->
        <div class="container mt--8 pb-5">
            <!-- Table -->
            <div class="row justify-content-center">
                <div class="col-lg-6 col-md-8">
                    <div class="card bg-secondary shadow border-0">
                        <div class="card-body px-lg-5 py-lg-5">
                            <div class="text-center text-muted mb-4">
                                <small>Sign In</small>
                            </div>
                            
                            <form method="POST" action="{{ route('login') }}" role="form">
                                @csrf
                                <div class="form-group">
                                    <div class="input-group input-group-alternative mb-3">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text"><i class="ni ni-key-25"></i></span>
                                        </div>
                                        <input class="form-control" name="email" placeholder="Email" type="text" value="{{ old('email') }}" required autofocus>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="input-group input-group-alternative">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text"><i class="ni ni-lock-circle-open"></i></span>
                                        </div>
                                        <input class="form-control" name="password" placeholder="Password" type="password" required>
                                    </div>
                                </div>
                                <div class="text-center">
                                    <button type="submit" class="btn btn-primary mt-4">Sign in</button>
                                </div>
                            </form>
                        </div>
                    </div>

                    {{--<div class="row mt-3">--}}
                        {{--<div class="col-6">--}}
                            {{--<a href="{{ route('password.request') }}" class="text-light"><small>Forgot password?</small></a>--}}
                        {{--</div>--}}
                        {{--<div class="col-6 text-right">--}}
                            {{--<a href="{{ route('register') }}" class="text-light"><small>Create new account</small></a>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                </div>
            </div>
        </div>

        <!-- Footer -->
        <footer class="py-5">
        <div class="container">
            <div class="row align-items-center justify-content-xl-between">
                <div class="col-xl-12">
                    <div class="copyright text-center text-xl-left text-muted">
                    &copy; 2018 -
                        Template by <a href="https://www.creative-tim.com" class="font-weight-bold ml-1" target="_blank">Creative Tim</a> |
                        Developed by <a href="https://www.linkedin.com/in/vedo-alfarizi-56a04b145/">Vedo Alfarizi</a>
                    </div>
                </div>
            </div>
        </div>
        </footer>
    </div>

    <script src="{{asset('js/jquery.min.js')}}"></script>
    <script src="{{asset('js/bootstrap.bundle.min.js')}}"></script>
    <script src="{{asset('js/argon.min.js')}}"></script>

</body>
</html>