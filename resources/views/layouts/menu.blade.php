@if(Auth::user()->role == 0)
<ul class="navbar-nav">   
    <li class="nav-item {{ Request::is('admin/dashboard*') ? 'active' : '' }}">
        <a class="nav-link" href="{{route('admin.dashboard')}}">
            <i class="ni ni-tv-2 text-primary"></i> Dashboard
        </a>
    </li> 
    <li class="nav-item {{ Request::is('admin/drinking*') ? 'active' : '' }}">
        <a class="nav-link" href="{{route('admin.drinking.index')}}">
            <i class="ni ni-tv-2 text-primary"></i> Air Minum
        </a>
    </li>
    {!! Form::open(['route' => 'admin.chart.drink', 'method' => 'GET']) !!}
    <li class="nav-item {{ Request::is('admin/chart/drink*') ? 'active' : '' }}">
        <input type="hidden" name="year" value="{{date('Y')}}">
        <input type="hidden" name="type" value="drink">
        <button class="nav-link btn btn-white btn-block" type="submit"><i class='ni ni-tv-2 text-primary'></i> Grafik Air Minum</button>
    </li>
    {!! Form::close() !!}
    <li class="nav-item {{ Request::is('admin/waste*') ? 'active' : '' }}">
        <a class="nav-link" href="{{route('admin.waste.index')}}">
            <i class="ni ni-tv-2 text-primary"></i> Air Limbah
        </a>
    </li>
    {!! Form::open(['route' => 'admin.chart.waste', 'method' => 'GET']) !!}
    <li class="nav-item {{ Request::is('admin/chart/waste*') ? 'active' : '' }}">
        <input type="hidden" name="year" value="{{date('Y')}}">
        <input type="hidden" name="type" value="waste">
        <button class="nav-link btn btn-white btn-block" type="submit"><i class='ni ni-tv-2 text-primary'></i> Grafik Air Limbah</button>
    </li>
    {!! Form::close() !!}
    <li class="nav-item {{ Request::is('admin/clean*') ? 'active' : '' }}">
        <a class="nav-link" href="{{route('admin.clean.index')}}">
            <i class="ni ni-tv-2 text-primary"></i> Air Bersih
        </a>
    </li>
    {!! Form::open(['route' => 'admin.chart.clean', 'method' => 'GET']) !!}
    <li class="nav-item {{ Request::is('admin/chart/clean*') ? 'active' : '' }}">
        <input type="hidden" name="year" value="{{date('Y')}}">
        <input type="hidden" name="type" value="clean">
        <button class="nav-link btn btn-white btn-block" type="submit"><i class='ni ni-tv-2 text-primary'></i> Grafik Air Bersih</button>
    </li>
    {!! Form::close() !!}
</ul>
@elseif(Auth::user()->role == 1)
<ul class="navbar-nav">   
    <li class="nav-item {{ Request::is('dashboard*') ? 'active' : '' }}">
        <a class="nav-link" href="{{route('dashboard')}}">
            <i class="ni ni-tv-2 text-primary"></i> Dashboard
        </a>
    </li> 
</ul>
@endif
<ul class="navbar-nav">    
    <li class="nav-item">
        <a class="nav-link" href="{{ route('logout') }}" onclick="event.preventDefault();
        document.getElementById('logout-form').submit();">
            <i class="ni ni-user-run text-pink"></i> Logout
        </a>
        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
            @csrf
        </form>
    </li>
</ul>