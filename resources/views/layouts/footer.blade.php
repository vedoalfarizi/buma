<footer class="footer">
    <div class="row align-items-center justify-content-xl-between">
        <div class="col-xl-6">
        <div class="copyright text-center text-xl-left text-muted">
            &copy; 2018 Template Design by<a href="https://www.creative-tim.com" class="font-weight-bold ml-1" target="_blank">Creative Tim</a>
        </div>
        </div>
        <div class="col-xl-6">
            <div class="copyright text-center text-xl-right text-muted">
                Developed by<a href="https://www.linkedin.com/in/vedo-alfarizi-56a04b145/" class="font-weight-bold ml-1" target="_blank">Vedo Alfarizi</a>
            </div>
        </div>
    </div>
</footer>